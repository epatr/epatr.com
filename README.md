# [epatr.com](http://epatr.com)

## Style Guide

![#B4AF91](http://placehold.it/150/B4AF91/000000?text=B4AF91)
![#C03000](http://placehold.it/150/C03000/FFFFFF?text=C03000)

- Link color: \#B4AF91
- Link hover: \#C03000
- Headers: [Montserrat](https://www.google.com/fonts/specimen/Montserrat)
- Paragraph: [Source Sans Pro](https://www.google.com/fonts/specimen/Source+Sans+Pro)


## Changelog

### February 2016

Minor text updates to include some Bandcamp projects I've been a part of.


### December 2015

Bought epatr.com and switched over the CNAME.


### October 2015

Minor changes to some text. Moved the site to Github Pages because I had no need for a shared hosting account really.


### September 2015

Removed the old HTML5 Boilerplate crud. I started a Wordpress install at http://blog.espatrick.com that I plan to use
as my portfolio rather than just making a boring grid of thumbnails that barely represent the work I actually put in. I'm using
[Bourbon](https://github.com/thoughtbot/bourbon)
and
[Neat](https://github.com/thoughtbot/neat)
for my Sass mixins and grid layout. I'm testing out working exclusively in [Brackets](http://brackets.io) and
seeing how I like [Prepros](https://prepros.io).


### March 2015

Playing around with [HTML5 Boilerplate](https://html5boilerplate.com/). All I really wanted was a list of links to current projects.
